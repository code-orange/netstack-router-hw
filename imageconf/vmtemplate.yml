---
name: netstack-router-hw
provider:
  name: kvm
bootstrapper:
  workspace: /target/os-images
  mirror: https://debian-archive.packages.managed-infra.com/debian/
  include_packages:
   - initramfs-tools
   - linux-headers-amd64
   - whois
   - psmisc
   - apt-transport-https
   - ca-certificates
   - ssl-cert
   - default-jre
   - default-jdk
system:
  release: buster
  architecture: amd64
  hostname: netstack-router-hw
  bootloader: grub
  charmap: UTF-8
  locale: en_US
  timezone: Europe/Berlin
volume:
  backing: raw
  partitions:
    type: gpt
    boot:
      filesystem: ext4
      size: 1GiB
      mountopts:
        - defaults
        - discard
        - noatime
        - errors=remount-ro
    swap:
      size: 2GiB
      mountopts:
        - sw
        - discard
        - noatime
        - errors=remount-ro
    root:
      filesystem: ext4
      size: 8GiB
      mountopts:
        - defaults
        - discard
        - noatime
        - errors=remount-ro
    var/log:
      filesystem: ext4
      size: 3GiB
      mountopts:
        - defaults
        - discard
        - noatime
        - errors=remount-ro
packages:
  install:
    # custom standard packages
    - chrony
    - unp
    - locate
    - lsscsi
    - sg3-utils
    - unattended-upgrades
    - debsecan
    - openipmi
    - freeipmi
    - ipmitool
    - iucode-tool
    - amd64-microcode
    - intel-microcode
    - fwupd
    - usbip
    - vim
    - nano
    - vim-nox
    - unzip
    - rsync
    - davfs2
    - bzip2
    - zstd
    - binutils
    - sudo
    - anacron
    - incron
    - screen
    - tree
    - zram-tools
    - haveged
    - needrestart
    - rng-tools
    - resolvconf
    - wget
    - curl
    # network tools
    - ethtool
    - wpasupplicant
    - mstflint
    - iproute2
    - dante-client
    - proxychains
    - proxychains4
    - stunnel4
    - net-tools
    - mtr
    - traceroute
    - ebtables
    - tcpdump
    - snmpd
    - snmp
    - snmp-mibs-downloader
    - lldpd
    - ipxe
    - ipxe-qemu
    - memtest86+
    # ntop
    # TODO: - pfring
    # TODO: - nprobe
    - ntopng
    - ntopng-data
    # TODO: - n2disk
    # TODO: - cento
    # TODO: - nbox
    - sngrep
    - speedtest
    # backup
    - borgbackup
    - proxmox-backup-client
    - etckeeper
    # email smarthost
    - dma
    - mailutils
    - mutt
    # cloud specific packages
    - cloud-init
    - salt-minion
    - puppet-agent
    - chef
    # user access management (directory)
    - freeipa-client
    - sssd
    - sssd-dbus
    - libsss-sudo
    - libsss-idmap0
    - libsss-nss-idmap0
    - krb5-config
    - krb5-k5tls
    - krb5-user
    # - libpam-krb5
    - libpam-ccreds
    # monitoring
    - lm-sensors
    - nvme-cli
    - apcupsd
    - smartmontools
    - monit
    - smstools
    - icinga2
    - lynis
    - trivy
    - fio
    - zabbix-agent
    - omi
    - scx
    - watchdog
    - monitoring-plugins
    - monitoring-plugins-basic
    - monitoring-plugins-standard
    - monitoring-plugins-btrfs
    - nagios-plugins-contrib
    - nagios-snmp-plugins
    - nagios-plugins-rabbitmq
    - nagios-check-xmppng
    - libmonitoring-plugin-perl
    - libipc-run-perl
    - liblist-moreutils-perl
    - filebeat
    - metricbeat
    - auditbeat
    - heartbeat-elastic
    - packetbeat
    - rancid
    # security
    - cracklib-runtime
    - wazuh-agent
    - auditd
    - audispd-plugins
    - ipset
    - conntrack
    - shorewall
    - shorewall6
    - rkhunter
    - fail2ban
    - crowdsec
    - crowdsec-firewall-bouncer-iptables
    # system management
    - aptitude
    - rpm
    - apt-file
    - apt-show-versions
    - apt-listchanges
    - debsums
    - atop
    - iotop
    - nload
    - neofetch
    # cluster tools
    - pacemaker
    - heartbeat
    - corosync
    - keepalived
    - exabgp
    - python3
    - python3-dev
    - python3-venv
    - python3-pip
    - python3-setuptools
    - python3-wheel
    - python
    - python-pip
    - python-setuptools
    # OpenStack support
    - neutron-server
    - neutron-openvswitch-agent
    - neutron-bgp-dragent
    - neutron-dhcp-agent
    - neutron-l2gateway-agent
    - neutron-l3-agent
    - neutron-lbaasv2-agent
    - neutron-linuxbridge-agent
    # router packages (VPN)
    - openvpn
    - easy-rsa
    - strongswan
    - xl2tpd
    - tinc
    - fastd
    #- wireguard
    - arp-scan
    # router packages (dial-in)
    - pppoeconf
    - ppp
    - pppoe
    # router packages (network)
    - vlan
    - bridge-utils
    - ifenslave
    - openvswitch-common
    - openvswitch-switch
    # router packages (DNS)
    - dnsdist
    - pdns-recursor
    # router packages (DHCP)
    - radvd
    - avahi-daemon
    - avahi-discover
    - avahi-utils
    - isc-kea-admin
    - isc-kea-ctrl-agent
    - isc-kea-dhcp4-server
    - isc-kea-dhcp6-server
    - isc-kea-dhcp-ddns-server
    - isc-dhcp-relay
    # router packages - FRRouting (FRR)
    - frr
    - frr-pythontools
    # router packages (BGP)
    - bird
    # router packages (proxy)
    - squid
    - dante-server
    - privoxy
    - nginx-full
    - nginx-extras
    - libnginx-mod-http-brotli
    - haproxy
    # pomerium
    - pomerium
    - pomerium-cli
    # router packages (privacy)
    - tor
    # print services
    - task-print-server
    - cups
    - cups-daemon
    - cups-client
    - cups-core-drivers
    - printer-driver-all-enforce
    # mesh packages
    - batctl
    # OpenNMS
    #- opennms-minion
    # OpenLDAP
    - slapd
    - ldap-utils
    - ldapscripts
    # administration
    #- webmin
    # web stats
    - awstats
    - webalizer
    - goaccess
    - geoip-database
    - libclass-dbi-mysql-perl
    - libtimedate-perl
    # RAID hardware - card management
    - 3dm2
    - 3ware-status
    - aacraid-status
    - adaptec-storage-manager-agent
    - adaptec-universal-storage-mib
    - adaptec-universal-storage-snmpd
    - arcconf
    - cciss-vol-status
    - dellmgr
    - hpacucli
    - hrconf
    - lsiutil
    - megacli
    - megaclisas-status
    - megactl
    - megaide-status
    - megamgr
    - megaraid-status
    - mpt-status
    - sas2ircu
    - sas2ircu-status
    - tw-cli
  install_standard: false
  mirror: https://debian-archive.packages.managed-infra.com/debian/
  security: https://debian-archive.packages.managed-infra.com/debian-security/
  sources:
    debian-backports:
      - deb https://debian-archive.packages.managed-infra.com/debian buster-backports main contrib non-free
    debian-fasttrack:
      - deb https://debian-fasttrack.packages.managed-infra.com/debian/ buster-fasttrack main contrib
      - deb https://debian-fasttrack.packages.managed-infra.com/debian/ buster-backports main contrib
    pbs-client:
      - deb https://proxmox.packages.managed-infra.com/debian/pbs-client buster main
    icinga:
      - deb https://icinga.packages.managed-infra.com/debian icinga-buster main
    zabbix:
      - deb https://zabbix.packages.managed-infra.com/zabbix/6.5/debian buster main
      - deb-src https://zabbix.packages.managed-infra.com/zabbix/6.5/debian buster main
    microsoft:
      - deb https://microsoft.packages.managed-infra.com/debian/10/prod buster main
    aquasecurity:
      - deb https://aquasecurity.packages.managed-infra.com/trivy-repo/deb buster main
    crowdsec:
      - deb https://packagecloud.packages.managed-infra.com/crowdsec/crowdsec/any/ any main
    newrelic-infra:
      - deb https://newrelic.packages.managed-infra.com/infrastructure_agent/linux/apt buster main
    hwraid:
      - deb https://hwraid.packages.managed-infra.com/debian buster main
    opennms:
      - deb https://opennms-deb.packages.managed-infra.com stable main
      - deb-src https://opennms-deb.packages.managed-infra.com stable main
    webmin:
      - deb https://webmin.packages.managed-infra.com/download/repository sarge contrib
    elasticsearch:
      - deb https://elastic.packages.managed-infra.com/packages/8.x/apt stable main
    puppet:
      - deb https://puppetlabs-apt.packages.managed-infra.com buster puppet
    chef:
      - deb https://chef.packages.managed-infra.com/repos/apt/stable buster main
    goaccess:
      - deb https://goaccess.packages.managed-infra.com/ buster main
    suryorg:
      - deb https://sury.packages.managed-infra.com/nginx-mainline/ buster main
    pomerium:
      - deb https://cloudsmith.packages.managed-infra.com/public/pomerium/pomerium/deb/debian buster main
      - deb-src https://cloudsmith.packages.managed-infra.com/public/pomerium/pomerium/deb/debian buster main
    powerdnsdist:
      - deb https://powerdns.packages.managed-infra.com/debian buster-dnsdist-19 main
    pdnsrecursor:
      - deb https://powerdns.packages.managed-infra.com/debian buster-rec-52 main
    openvpn:
      - deb https://openvpn-as.packages.managed-infra.com/as/debian buster main
      - deb https://openvpn.packages.managed-infra.com/debian/openvpn/stable buster main
    isc:
      - deb https://cloudsmith.packages.managed-infra.com/public/isc/kea-2-4/deb/debian buster main
      #- deb-src https://cloudsmith.packages.managed-infra.com/public/isc/kea-2-4/deb/debian buster main
    ntop:
      - deb https://ntop.packages.managed-infra.com/apt-stable/buster/ x64/
      - deb https://ntop.packages.managed-infra.com/apt-stable/buster/ all/
    ookla:
      - deb https://ookla-speedtest-cli.packages.managed-infra.com/ookla/speedtest-cli/debian/ buster main
      - deb-src https://ookla-speedtest-cli.packages.managed-infra.com/ookla/speedtest-cli/debian/ buster main
    haproxy:
      - deb https://debian-haproxy.packages.managed-infra.com buster-backports-2.6 main
    saltstack:
      - deb https://saltstack.packages.managed-infra.com/artifactory/saltproject-deb/ stable main
    openstack:
      - deb https://debian-osbpo.packages.managed-infra.com/osbpo buster-train-backports main
      - deb https://debian-osbpo.packages.managed-infra.com/osbpo buster-train-backports-nochange main
    tor:
      - deb https://torproject.packages.managed-infra.com/torproject.org buster main
      - deb-src https://torproject.packages.managed-infra.com/torproject.org buster main
    frr:
      - deb https://frrouting.packages.managed-infra.com/frr buster frr-stable
    wazuh:
      - deb https://wazuh.packages.managed-infra.com/4.x/apt/ stable main
    cisofy:
      - deb https://cisofy.packages.managed-infra.com/community/lynis/deb/ stable main
    falcosecurity:
      - deb https://falco.packages.managed-infra.com/packages/deb stable main
  preferences:
    haproxy:
      - package: haproxy*
        pin: origin debian-haproxy.packages.managed-infra.com
        pin-priority: 600
    rancid:
      - package: rancid
        pin: release n=buster-backports
        pin-priority: 990
    pdnsrecursor:
      - package: pdns-recursor
        pin: origin powerdns.packages.managed-infra.com
        pin-priority: 600
  components:
    - main
    - contrib
    - non-free
  trusted-keys:
    - trusted-repo-keys/trusted-repo-keys.gpg
  apt.conf.d:
    00InstallRecommends: >-
      APT::Install-Recommends "false";
      APT::Install-Suggests   "false";
    00RetryDownload: 'APT::Acquire::Retries "3";'
plugins:
  tmpfs_workspace: {}
  admin_user:
    username: admin
    password: ch4ng3m3
    pubkey: ../authorized_keys
  apt_proxy:
    address: 127.0.0.1
    port: 8000
  debconf: >-
    d-i pkgsel/install-language-support boolean false
    popularity-contest popularity-contest/participate boolean false
    resolvconf resolvconf/linkify-resolvconf boolean true
  file_copy:
    files:
      - { src: ../copy-to-filesystem.tar.gz,
          dst: /opt/copy-to-filesystem.tar.gz,
          permissions: -rwxrwxrwx,
          owner: root,
          group: root }
  commands:
    commands:
      - ['chroot {root} tar --strip-components 1 -xvzf /opt/copy-to-filesystem.tar.gz -C /']
      - ['chroot {root} chmod 770 /usr/sbin/cmdb-deploy']
      - ['chroot {root} chmod 770 /usr/bin/cleanup_image']
      - ['chroot {root} chmod 770 /usr/sbin/neutron_services_disable']
      - ['chroot {root} /usr/sbin/neutron_services_disable']
      - ['chroot {root} /usr/bin/cleanup_image']
  minimize_size:
    zerofree: true
    apt:
      autoclean: true
